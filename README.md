Create a fetch.json file in the same directory with 4 lines:
```
SERVER
LOGIN
PASSWORD
SUBJECT_TAG
```

This will be used to connect using IMAP4 SSL to the default mailbox, and gather
all emails that have SUBJECT_TAG in them, to generate a JSON file from the
matching emails.

Emails are expected to be plaintext, and have 3 lines :
```
Position (format: [LAT, LONG])
Date (format: 2022-07-05T18:16+02:00)
Message (free form)
```
