#! /usr/bin/env python3

"""Fetch emails, look for new ones, update position"""

from imaplib import IMAP4_SSL
import email
from email import policy
import json


# Simple fetch.conf file with 2 lines : login for email, password for the mailbox
with open('fetch.conf', 'r') as f:
    login = f.readline().strip()
    password = f.readline().strip()
    server = f.readline().strip()
    subject_tag = f.readline().strip()

SUBJECT_TAG = subject_tag
DEBUG = True

# First point on the chart, with the basic structure (date, position, message)
depart =  dict(date="2022-07-05T16:00+02:00", position=[46.77643,-56.17363], message="On n'est pas partis !")
positions = [ depart ]

# IMAP context
with IMAP4_SSL(server) as M:
    # Connect, login, select "default mailbox"
    M.login(login, password)
    M.enable("UTF8")
    M.select()
    # Filter emails based on the subject tag
    rt, messages = M.search(None, f'(SUBJECT "{SUBJECT_TAG}")')
    
    # Iterate over all returned mails
    for mailid in messages[0].decode().split()[-2:]:
        t, d = M.fetch(mailid, '(RFC822)')
        # Parse the email
        msg = email.message_from_bytes(d[0][1], policy=email.policy.default)
        date = msg.get("Date")
        # Read the body carelessly : we assume it's not MIME splitted and that it is plaintext
        body = msg.get_payload(decode=True).split()
        # First line is supposed to be position, written LAT, LONG
        position = body[0].decode().split(',')
        x = float(position[0])
        y = float(position[1])
        # Second line should be the date of the waypoint
        pdate = body[1].decode()
        message = ""
        # And all remaining lines should be the message of the moment
        for m in body[2:]:
            message += m.decode() + " "

        # Add all info to the positions list
        positions += [dict(date=pdate, position=[x, y], message=message)]
if DEBUG:
    for p in positions:
        for param in 'date', 'position', 'message':
            print(p[param], type(p[param]))
    print(type(positions))
    print(positions)

# Write a .json file with all the data
with open('fetched.json', 'w') as jf:
    json.dump(positions, jf, indent=2)